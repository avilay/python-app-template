import csv
import logging
import os
import pkg_resources
import dateutil.parser as dtparser
from python_app_template.globals import *

logger = logging.getLogger(__name__)


class PackOne:
    def __init__(self):
        self._grade = config['PACK']['grade']
        print('Setting grade to {}'.format(self._grade))

    def process_img(self):
        logger.debug('Processing img')
        imgfile = pkg_resources.resource_filename(__package__, 'res/picture.jpg')
        return os.stat(imgfile)

    def process_csvs(self):
        logger.debug('Processing csvs')
        rows = []

        data_1 = pkg_resources.resource_filename(__package__, 'res/data_1.csv')
        with open(data_1) as f1:
            reader = csv.DictReader(f1)
            for row in reader:
                row['published_on'] = dtparser.parse(row['published_on'])
                rows.append(row)

        data_2 = pkg_resources.resource_filename(__package__, 'res/data_2.csv')
        with open(data_2) as f2:
            reader = csv.DictReader(f2)
            for row in reader:
                row['tweeted_on'] = dtparser.parse(row['tweeted_on'])
                rows.append(row)

        return rows
