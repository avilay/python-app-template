import os.path as path
import logging
from python_app_template.globals import *

logger = logging.getLogger(__package__)


def config_log():
    logformat = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
    formatter = logging.Formatter(logformat)

    fh = logging.FileHandler(config['LOGGING']['logpath'])
    fh.setLevel(getattr(logging, config['LOGGING']['filelevel'].upper()))
    fh.setFormatter(formatter)

    ch = logging.StreamHandler()
    ch.setLevel(getattr(logging, config['LOGGING']['consolelevel'].upper()))
    ch.setFormatter(formatter)

    logger.setLevel(logging.DEBUG)
    logger.addHandler(fh)
    logger.addHandler(ch)


def startup(setup_cliargs):
    args = setup_cliargs().parse_args()

    if args.version:
        print('{} {}'.format(__package__, version))
        exit()

    configfile = '/etc/{}/conf/{}.ini'.format(__package__, args.env)
    if not path.exists(configfile):
        print('Config file {} does not exist!'.format(configfile))
        exit(1)
    config.read(configfile)

    config_log()
    logger.info('Starting {} in the {} env with config file {}'.format(__package__, args.env, configfile))
    return args
