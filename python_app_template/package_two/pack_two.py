import logging

logger = logging.getLogger(__name__)


class PackTwo:
    def do_something(self):
        logger.debug('Doing something')
        print('\nAnd now this is doing something useful!')
