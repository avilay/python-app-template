import argparse
import logging
import simplecrypt
import base64
import os
from python_app_template.globals import *
from python_app_template.package_one.pack_one import PackOne
from python_app_template.package_two.pack_two import PackTwo
from python_app_template.startup import startup

logger = logging.getLogger(__name__)


def setup_cliargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', action='store_true', help='Prints the version of {}.'.format(__package__))
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'test', 'prod'], help='Set the deployment environment. Can be one of dev, test, or prod')
    parser.add_argument('-f', '--foo', help='Some app specific argument')
    return parser


def main():
    args = startup(setup_cliargs)

    # access args
    print('Value of --foo is {}'.format(args.foo))

    # export SECRET_KEY=sshh
    if 'SECRET_KEY' not in os.environ:
        print('SECRET_KEY has not been set!')
        exit(1)
    secret_key = os.environ['SECRET_KEY']
    connstr = simplecrypt.decrypt(secret_key, base64.b64decode(config['SECRETS']['connstr'])).decode('utf-8')
    print('connstr = {}'.format(connstr))

    try:
        p1 = PackOne()
        for row in p1.process_csvs():
            print(row)
        print(p1.process_img())
    except FileNotFoundError as e:
        print('Unable to read data files!')
    p2 = PackTwo()
    p2.do_something()


if __name__ == '__main__':
    main()
