import pwd
from setuptools import setup, find_packages
from setuptools.command.install import install
from python_app_template.globals import *
import os.path as path
import os
from crontab import CronTab


class PostInstall(install):
    def create_runtime_path(self):
        logpath = '/etc/python_app_template/logs'
        if not path.exists(logpath):
            os.makedirs(logpath)
        whoami = pwd.getpwnam(os.getlogin()).pw_uid
        for root, dirs, files in os.walk('/etc/python_app_template'):
            for dir in dirs:
                os.chown(path.join(root, dir), whoami, -1)
            for file in files:
                os.chown(path.join(root, file), whoami, -1)

    def create_crontab(self):
        cron = CronTab(user=os.getlogin())


    def run(self):
        install.run(self)
        self.create_runtime_path()
        self.create_crontab()


setup(
    name='python-app-template',
    version=version,
    description='Python application template',
    packages=find_packages(exclude=['tests']),
    install_requires=['simple-crypt'],
    package_data={
        'python_app_template.package_one': ['res/*.csv', 'res/*.jpg'],
    },
    data_files=[
        ('/etc/python_app_template/conf/', ['conf/dev.ini', 'conf/prod.ini', 'conf/test.ini'])
    ],
    entry_points={
        'console_scripts': [
            'python_app_template=python_app_template.python_app_template:main'
        ]
    },
    cmdclass={'install': PostInstall}
    # It is a good practice to also specify the following params -
    # description
    # url
    # author
    # author_email
    # license
)
